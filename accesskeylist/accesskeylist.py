""" This script will print the list of access keys more than 90 days old.
Author: Christopher Lundy
Requirements: boto3 package installed, standard libraries - datetime, csv, and aws credentials configured.
"""

import boto3
import datetime
import csv

iam_client = boto3.client('iam')

def access_key(user):

    """
    List the access key(s) of every user. Then, iterate over the keys and pass the created date to time_diff()
    CreateDate of Access Key is a datetime object. Passing it as an input to time_diff func to get the age in days.
       Parameters: user (string) : Username of IAM user
    """
    keydetails=iam_client.list_access_keys(UserName=user)

    # Some users may have more than one access key. Iterate keys and list the details of active access keys.
    for keys in keydetails['AccessKeyMetadata']:
        if keys['Status']=='Active' and (time_diff(keys['CreateDate'])) >= 90:
            print (keys['UserName'],keys['AccessKeyId'], time_diff(keys['CreateDate']),sep=',')

def time_diff(keycreatedtime):
    # Getting the current time in utc format
    now=datetime.datetime.now(datetime.timezone.utc)
	# Calculate the diff between the two datetime objects.
    diff=now-keycreatedtime
    # Return the difference in days
    return diff.days


if __name__ == '__main__':
    """ In the main function, fetch the users from IAM and pass the username to access_key() """

    # List the users, set a MaxItems response to limit results. This will return a dictionary response
    details = iam_client.list_users(MaxItems=300)
    # Header information
    print ("Username","AccessKey","KeyAge",sep=',')
    for user in details['Users']:
        access_key(user['UserName'])
