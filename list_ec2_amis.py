#Import the AWS boto3 library
import boto3
#Import the pprint library for output
import pprint

# Sets PrettyPrinter indent
pp = pprint.PrettyPrinter(indent=2)
# Set ec2_client variable to point to 'us-west-2' region_name value
ec2_client = boto3.client('ec2', region_name='us-west-2') # Change as appropriate
# List all AMIs that are in the account. If you leave out 'self', it will list all publicly-accessible AMIs
images = ec2_client.describe_images(Owners=['self'])
# Print the results out(??)
