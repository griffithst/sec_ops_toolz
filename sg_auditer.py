import boto3
import pprint
import sys

#import json
#import click


pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')
elbv2_client = boto3.client('elb')

def get_resources_from_sg(group_id):
    """Take a security group and find all associated resources and return a human readable id"""
    resource_name_list = []

    #Check ec2 instances
    instances_in_group = ec2_client.describe_instances(Filters=[{'Name': 'instance.group-id', 'Values': [group_id]}])

    for reservations in instances_in_group['Reservations']:
        for instance in reservations['Instances']:
            instance_id = instance['InstanceId']
            instance_name = 'no_name'

            #pp.pprint(instance)
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']

            resource_name_list.append(f'Instance {instance_name} - {instance_id}')

    #Check ELBs V1 only ie classic ELBs
    all_elbs = elbv2_client.describe_load_balancers()
    for lb in all_elbs['LoadBalancerDescriptions']:
        if group_id in lb['SecurityGroups']:

            #print(lb['DNSName'])
            #print(lb['SecurityGroups'])
            resource_name_list.append(f"ELB -- {lb['DNSName']}")


    #For later add RDS and ENIs and maybe ELBv2 for futureproofing


    return resource_name_list


def get_security_groups_for_resource(input_resource):
    """Take an aws resource figure out what it is and return its associated sgs"""
    if input_resource.startswith('i-'):
        print(f'{input_resource} is an instance id. Looking up security groups associated with instance.')
        instance = ec2_resource.Instance(input_resource)
        input_resource_sgs = instance.security_groups
        return input_resource_sgs
    #might have to bring click in to play here - no easy arn access for classic elbs so user might need to specify resource type
    #if input_resource.startswith()



def main():

    input_resource = sys.argv[1]

    input_resource_sgs = get_security_groups_for_resource(input_resource)
    #pp.pprint(input_resource_sgs)
    ingress_report = {}

    for sg in input_resource_sgs:
        security_group_id = sg['GroupId']
        securitygroup = ec2_resource.SecurityGroup(security_group_id)

        #pp.pprint(securitygroup.ip_permissions)
        
        for ingress_permission in securitygroup.ip_permissions:
            port_range = f"{ingress_permission['FromPort']} - {ingress_permission['ToPort']}"
            ingress_entity_list = []
            #print(port_range)

            #pp.pprint(ingress_permission['IpRanges'])
            for ipv4 in ingress_permission['IpRanges']:

                ingress_entity_list.append(ipv4['CidrIp'])
            for ipv6 in ingress_permission['Ipv6Ranges']:
                ingress_entity_list.append(ipv6['CidrIpv6'])
            for uidgp in ingress_permission['UserIdGroupPairs']:
                group_id = uidgp['GroupId']
                resource_name_list = get_resources_from_sg(group_id)

                for name in resource_name_list:
                    ingress_entity_list.append(name)
            for entity in ingress_entity_list:
                if entity not in ingress_report.keys():
                    ingress_report[entity] = [port_range]
                else:
                    ingress_report[entity].append(port_range)

    print(f"{input_resource} has security group ingress rules allowing for connections from:")
    pp.pprint(ingress_report)


if __name__ == "__main__":
    main()