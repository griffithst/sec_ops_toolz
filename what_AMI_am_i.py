import pprint
import boto3


pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')

def main():
    response = ec2_client.describe_instances()
    output_list = []


    for reservation in response['Reservations']:
        for instance in reservation['Instances']:

            instance_name = 'no_name'
            instance_ami_id = instance['ImageId']
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']

            try:
                ami_resource = ec2_resource.Image(instance_ami_id)
                ami_description = ami_resource.description

            except:
                ami_description = 'no description - custom AMI'


            if '2017' in ami_description:
                output_list.append(f"{instance_name} launched from {ami_description}")
    output_list.sort()
    pp.pprint(output_list)

if __name__ == "__main__":
    main()