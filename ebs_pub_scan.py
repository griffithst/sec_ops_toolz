import boto3
import pprint


pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
s3_client = boto3.client('s3')


def main():
    """Loop through all regions and check ebs vol snapshots for public (create volume permissions)"""

    response = s3_client.list_buckets()
    pp.pprint(response)

    for region in ec2_client.describe_regions()['Regions']:
        print(f"Checking Region {region['RegionName']}")
        regional_client=boto3.client('ec2', region_name=region['RegionName'])
        snapshots = regional_client.describe_snapshots(OwnerIds=['160972975875'])

        for snap in snapshots['Snapshots']:
            print(f"{snap['SnapshotId']} has the following create volume permissions")
            attributes=regional_client.describe_snapshot_attribute(Attribute='createVolumePermission',SnapshotId=snap['SnapshotId'])
            pp.pprint(attributes['CreateVolumePermissions'])

if __name__ == "__main__":
    main()