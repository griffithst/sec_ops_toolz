import boto3
import pprint
import sys

#import json
#import click
"""
This is just spaghetti code right now for some temp needs. if it becomes handy i'll merge a few of these scripts
and put them behind some Click CLI
"""

pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')
elbv2_client = boto3.client('elb')

def get_resources_from_sg(group_id):
    """Take a security group and find all associated resources and return a human readable id"""
    resource_name_list = []

    #Check ec2 instances
    instances_in_group = ec2_client.describe_instances(Filters=[{'Name': 'instance.group-id', 'Values': [group_id]}])

    for reservations in instances_in_group['Reservations']:
        for instance in reservations['Instances']:
            instance_id = instance['InstanceId']
            instance_name = 'no_name'

            #pp.pprint(instance)
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']

            resource_name_list.append(f'Instance {instance_name} - {instance_id}')

    #Check ELBs V1 only ie classic ELBs
    all_elbs = elbv2_client.describe_load_balancers()
    for lb in all_elbs['LoadBalancerDescriptions']:
        if group_id in lb['SecurityGroups']:
            resource_name_list.append(f"ELB -- {lb['DNSName']}")


    #For later add RDS and ENIs and maybe ELBv2 for futureproofing


    return resource_name_list

def find_all_instances_w_sg(input_security_group):
    des_sg_resp=ec2_client.describe_security_groups(Filters=[{'Name': 'ip-permission.group-id', 'Values': [input_security_group]}])
    pp.pprint(f'{input_security_group} is referenced in the following security groups:')

    for insg in des_sg_resp['SecurityGroups']:
        print('')
        print('***************************')
        sg_id=insg['GroupId']
        print(sg_id)
        resources_from_sg=get_resources_from_sg(insg['GroupId'])
        print(resources_from_sg)
        try:
            for item in insg['IpPermissions']:
                for ugid in item['UserIdGroupPairs']:
                    if ugid['GroupId'] == input_security_group:
                        print(f"for port range {item['FromPort']} to {item['ToPort']}")
        except:
            print('no uigp')
        print('***************************')
        print('')


def get_security_groups_for_resource(input_resource):
    """Take an aws resource figure out what it is and return its associated sgs"""
    if input_resource.startswith('i-'):
        print(f'{input_resource} is an instance id. Looking up security groups associated with instance.')
        instance = ec2_resource.Instance(input_resource)
        input_resource_sgs = instance.security_groups
        return input_resource_sgs
    #might have to bring click in to play here - no easy arn access for classic elbs so user might need to specify resource type
    #if input_resource.startswith()

def main():
    #what can i hit from this machine
    #take a sg id
    input_resource = sys.argv[1]
    instance_sgs = get_security_groups_for_resource(input_resource)

    print(f"Instance {input_resource} has the following security groups:")
    pp.pprint(instance_sgs)
    print("---------------------------")
    print("---------------------------")
    for sg in instance_sgs:
        find_all_instances_w_sg(sg['GroupId'])


if __name__ == "__main__":
    main()