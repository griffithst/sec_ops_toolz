import parsedmarc
import pprint
import json
import boto3
import zipfile
import gzip

pp = pprint.PrettyPrinter(indent=2)

def main():

    #Set default session profile and region for sandbox account. Access keys are pulled from ~/.aws/config and ~/.aws/credentials.
    #The 'profile_name' value comes from the header for the account in question in ~/.aws/config and ~/.aws/credentials
    boto3.setup_default_session(profile_name="sandbox", region_name="us-west-2")

    source_file = 'google.com!totalbrain.com!1589673600!1589759999.zip'
    destination_directory = '/tmp/'
    destination_file = 'compressed_report_file'

    #Define the s3 resource, the bucket name, and the file to download. It's hardcoded for now...
    s3_resource = boto3.resource('s3')
    s3_resource.Bucket('dmarc-parsing').download_file(source_file, f"{destination_directory}{destination_file}")

    #Extract xml
    outputxml = parsedmarc.extract_xml(f"{destination_directory}{destination_file}")

    #run parse dmarc analysis & convert output to json
    pd_report_output = parsedmarc.parse_aggregate_report_xml(outputxml)
    pd_report_jsonified = json.loads(json.dumps(pd_report_output))
    pp.pprint(pd_report_jsonified)

    #loop through results and find relivant status info and pass fail status
    dmarc_report_status = ''
    for record in pd_report_jsonified['records']:
        if False in record['alignment'].values():
            dmarc_report_status = 'Failed'
            #************ add logic for interpreting results

    #if fail, publish to sns
    if dmarc_report_status == 'Failed':

        message = "Your dmarc report failed boohoo"

        sns_resource = boto3.resource('sns')
        sns_topic = sns_resource.Topic('arn:aws:sns:us-west-2:112896196555:TestDMARC')
        sns_publish_response = sns_topic.publish(Message=message)


if __name__ == "__main__":
    main()
