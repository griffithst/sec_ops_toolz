import matplotlib.pyplot as plt
import networkx as nx
import pprint
import boto3

# some global resources
pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')
elb_client = boto3.client('elb')


class aware_security_group:
    """
    it would be nice if we could just extend the boto3 classes but that is not possible in an easy/clean way
    so we make a new class that can discover what it is attached to and also what resources have inbound access to it
    """
    def __init__(self, id):

        self.id = id
        self.attachments = self.get_attachments(self.id)
        self.ingress_rules = ec2_resource.SecurityGroup(id).ip_permissions

    def get_attachments(self, sg_id):
        """for any security group find find all associated resources and some metadata and return a dict"""
        attachments = {}
        # Check ec2 instances
        instances_in_group = ec2_client.describe_instances(Filters=[{'Name': 'instance.group-id', 'Values': [sg_id]}])

        for reservations in instances_in_group['Reservations']:
            for instance in reservations['Instances']:
                instance_id = instance['InstanceId']
                instance_name = 'no_name'


                for tag in instance['Tags']:
                    if tag['Key'] == 'Name':
                        instance_name = tag['Value']

                attachments[instance_id] = {'name': instance_name, 'type': 'ec2_instance'}

        # Check ELBs V1 only ie classic ELBs
        all_elbs = elb_client.describe_load_balancers()
        for lb in all_elbs['LoadBalancerDescriptions']:
            if sg_id in lb['SecurityGroups']:
                attachments[lb['LoadBalancerName']] = {'type': 'classic_elb', 'name': lb['LoadBalancerName']}

        return attachments

    def describe_inbound(self):
        """
            Given inbound rules create a dict of resources (elbs and ec2 atm) that have inbound access
            In hindsight... this should be moved to a global function not a class method IMO but if it works
            i'm leaving it alone for now
        """
        inbound_report = {}
        #securitygroup = ec2_resource.SecurityGroup(security_group)

        for ingress_permission in self.ingress_rules:
            port_range = f"{ingress_permission['FromPort']} - {ingress_permission['ToPort']}"
            ingress_entity_list = []

            for ipv4 in ingress_permission['IpRanges']:
                #print(ipv4)
                ipv4_cidrip = ipv4['CidrIp']
                if ipv4_cidrip not in inbound_report.keys():
                    inbound_report[ipv4_cidrip] = {'ports': [port_range], 'type': 'ip', 'name': ipv4_cidrip}
                else:
                    inbound_report[ipv4_cidrip]['ports'].append(port_range)

            for ipv6 in ingress_permission['Ipv6Ranges']:
                #print(ipv6)
                ipv6_cidrip = ipv6['CidrIpv6']
                if ipv6_cidrip not in inbound_report.keys():
                    inbound_report[ipv6_cidrip] = {'ports': [port_range], 'type': 'ip', 'name': ipv6_cidrip}
                else:
                    inbound_report[ipv6_cidrip]['ports'].append(port_range)

            # this is stupidly named in aws - security group rule based on sg membership
            for uidgp in ingress_permission['UserIdGroupPairs']:
                sg_id = uidgp['GroupId']
                resources_attached = self.get_attachments(sg_id)

                for resource in resources_attached:
                    if resource not in inbound_report.keys():
                        inbound_report[resource] = {'ports': [port_range], 'type': resources_attached[resource]['type'],
                                                    'name': resources_attached[resource]['name']}
                    else:
                        inbound_report[resource]['ports'].append(port_range)

        return inbound_report




def describe_instance_nodes(tag_key, tag_value):
    instance_nodes= {}
    di_response = ec2_client.describe_instances(Filters=[{'Name': f"tag:{tag_key}", 'Values': tag_value}])
    #pp.pprint(instances)
    for reservation in di_response['Reservations']:
        for instance in reservation['Instances']:
            instance_name = 'no_name'
            instance_id = instance['InstanceId']
            security_group_description = instance['SecurityGroups']
            instance_security_groups = []
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']

            for sg in security_group_description:
                instance_security_groups.append(sg['GroupId'])

            #node_list.append(f'{instance_name} - {instance_id}')
            instance_nodes[instance_id]={'name' : instance_name, 'security_groups' : instance_security_groups, 'type' : 'ec2_instance'}

    return instance_nodes

def describe_elbv1_nodes(tag_key, tag_value):
    """

    Args:
        tag_key: duh
        tag_value: a list of values for the key

    Returns: a dict of elbs that are tagged with their security groups as well as type and name attributes to match
    ec2 resources in other function

    """
    elbv1_nodes = {}

    all_elbs = elb_client.describe_load_balancers()
    for lb in all_elbs['LoadBalancerDescriptions']:
        tag_details = elb_client.describe_tags(LoadBalancerNames=[lb['LoadBalancerName']])
        #We can assume a list of 1 b/c we are only asking for one lb
        for tag in tag_details['TagDescriptions'][0]['Tags']:
            if tag['Key'] == tag_key and tag['Value'] in tag_value:
                elbv1_nodes[lb['LoadBalancerName']] = {'security_groups' : lb['SecurityGroups'], 'type': 'elb_classic', 'name': lb['LoadBalancerName'] }

    return elbv1_nodes


def main():

    tag_key = 'BrainGroup1'
    tag_value = ['prod-one', 'prod-two', 'prod-net']
    #network_options = 'include_external_nodes'
    network_options = 'exclude_external_nodes'
    output_file_name = 'default'

    #query services to find tagged resources and key metadata
    print("GATHERING DATA...")
    instance_details = describe_instance_nodes(tag_key, tag_value)
    #pp.pprint(instance_details)

    elbv1_details = describe_elbv1_nodes(tag_key, tag_value)
    #pp.pprint(elbv1_details)

    #merge the output dicts into 1 master (might add in more resource types to gather)
    resource_details = {}
    resource_details.update(instance_details)
    resource_details.update(elbv1_details)

    print("Gathered the following resources: \n")
    pp.pprint(resource_details)
    print("\n")
    print("********************************")

    #enrich the resource details dict w/ data on what resources have inbound access
    print("Enriching the resources with inbound connection data. This can take a few minutes.")

    for resource in resource_details:
        resource_name = resource_details[resource]['name']
        resource_security_groups = resource_details[resource]['security_groups']

        combined_inbound = {}
        for security_group_id in resource_security_groups:
            a_sg = aware_security_group(security_group_id)
            combined_inbound.update(a_sg.describe_inbound())

        resource_details[resource]['inbound'] = combined_inbound

    print('Enriched Resources:')
    pp.pprint(resource_details)
    print("********************************")


    print('Build nodes and edges and graph them')
    # from the enriched dict built the nodes & edges, we will use name for our node key b/c it is more descriptive
    nodes = {}

    for resource_key, resource_value in resource_details.items():
        node_name = resource_value['name']
        edges = []
        for edge_key, edge_value in resource_value['inbound'].items():
            edge_name = edge_value['name']
            edges.append(edge_name)
        nodes[node_name]=edges
    print("Built nodes and edges:")
    pp.pprint(nodes)

    print("Graphing the nodes and Outputting files.")
    #graph the nodes and output a simple graphic and a graphml file for use in things like cytoscape

    Graph = nx.DiGraph()

    for node_key, node_value in nodes.items():
        Graph.add_node(node_key)
        for edge in node_value:
           sconnects to an external node (ie not in the tagged resources) if we add it as an additional node or not
            if network_options == 'include_external_nodes':
                Graph.add_edge(edge, node_key)
            else:
                if edge in nodes.keys():
                    Graph.add_edge(edge, node_key)


    nx.write_graphml(Graph, f"{output_file_name}.graphml")

    nx.draw_circular(Graph, node_size=7, font_size=6, linewidths=0.2, with_labels=True)
    plt.savefig(f"{output_file_name}", pad_inches=3)

    print(f"Output files {output_file_name}.graphml and {output_file_name}.png")
    print("DONE!!!")


if __name__ == "__main__":
    main()