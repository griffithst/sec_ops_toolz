import boto3
import pprint
import sys

#import json
#import click


pp = pprint.PrettyPrinter(indent=2)
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')
elbv2_client = boto3.client('elb')


def find_unrestricted_sgs():
    dsg = ec2_client.describe_security_groups()
    # pp.pprint(dsg)

    sg_report = {}

    for sec_groups in dsg['SecurityGroups']:

        sg_id = sec_groups['GroupId']
        sg_open_ports_list = []

        for ip_perms in sec_groups['IpPermissions']:
            sg_availability = 'closed'
            port_range = f"{ip_perms['FromPort']} - {ip_perms['ToPort']}"
            for ipv4_range in ip_perms['IpRanges']:
                if ipv4_range['CidrIp'] == '0.0.0.0/0':
                    sg_availability = 'open'
            for ipv6_range in ip_perms['Ipv6Ranges']:
                if ipv6_range['CidrIpv6'] == '::/0':
                    sg_availability = 'open'

            if sg_availability == 'open':
                sg_open_ports_list.append(port_range)


        if len(sg_open_ports_list) > 0:
            sg_report[sg_id] = sg_open_ports_list

    
    return sg_report

def get_resources_from_sg(group_id):
    """Take a security group and find all associated resources and return a human readable id"""
    resource_name_list = []

    #Check ec2 instances
    instances_in_group = ec2_client.describe_instances(Filters=[{'Name': 'instance.group-id', 'Values': [group_id]}])

    for reservations in instances_in_group['Reservations']:
        for instance in reservations['Instances']:
            instance_id = instance['InstanceId']
            instance_name = 'no_name'

            #pp.pprint(instance)
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']

            resource_name_list.append(f'Instance {instance_name} - {instance_id}')

    #Check ELBs V1 only ie classic ELBs
    all_elbs = elbv2_client.describe_load_balancers()
    for lb in all_elbs['LoadBalancerDescriptions']:
        if group_id in lb['SecurityGroups']:

            #print(lb['DNSName'])
            #print(lb['SecurityGroups'])
            resource_name_list.append(f"ELB -- {lb['DNSName']}")


    #For later add RDS and ENIs and maybe ELBv2 for futureproofing


    return resource_name_list


def main():

    sg_report = find_unrestricted_sgs()

    for sg_id in sg_report:
        print(f"{sg_id} security group is open to inbound public traffic on ports {sg_report[sg_id]}")
        print("it is associated with:")
        resource_name_list = get_resources_from_sg(sg_id)
        pp.pprint(resource_name_list)


if __name__ == "__main__":
    main()